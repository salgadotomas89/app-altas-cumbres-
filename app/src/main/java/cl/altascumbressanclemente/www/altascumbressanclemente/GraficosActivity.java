package cl.altascumbressanclemente.www.altascumbressanclemente;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.DataSet;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;

import java.util.ArrayList;

public class GraficosActivity extends AppCompatActivity {
    private BarChart barChart;
    private String[] etiquetas = new String[]{"nota 1","nota 2","nota 3"};
    private int[] notas = new int[]{43,60,50};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_graficos);


    }

    private void crearGrafico(){
        barChart.animateY(3000);
        barChart.setDrawGridBackground(false);
        barChart.setData(getBarData());
        barChart.getDescription().setText("");

        Legend legend = barChart.getLegend();
        legend.setEnabled(false);
        ejeX(barChart.getXAxis());
        ejeYDerecho(barChart.getAxisRight());
        ejeYIzquierdo(barChart.getAxisLeft());
    }

    private DataSet getData(DataSet dataSet){
        dataSet.setValueTextSize(12);
        return dataSet;
    }

    private BarData getBarData(){
        BarDataSet barDataSet = (BarDataSet) getData(new BarDataSet(getBarEntries(),""));
        BarData barData = new BarData(barDataSet);
        return  barData;
    }

    private ArrayList<BarEntry> getBarEntries(){
        ArrayList<BarEntry> entries =  new ArrayList<>();
        for(int i=0;i<notas.length;i++){
            entries.add(new BarEntry(i,notas[i]));
        }
        return entries;
    }

    private void ejeX(XAxis eje){
        eje.setGranularityEnabled(true);
        eje.setPosition(XAxis.XAxisPosition.BOTTOM);
        eje.setValueFormatter(new IndexAxisValueFormatter(etiquetas));
        eje.setTextSize(14);
    }

    private void ejeYDerecho(YAxis ejeY){
        ejeY.setEnabled(false);
    }

    private void ejeYIzquierdo(YAxis ejeY){
        ejeY.setSpaceTop(70);

    }




}
