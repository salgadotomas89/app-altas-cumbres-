package cl.altascumbressanclemente.www.altascumbressanclemente;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.Map;

public class IngresarAlumnoActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    EditText rut,nombre,ciclo,numeroLista;//campos ingresados por el usuario
    Spinner spinnerCursos;//lista para los cursos
    String[] cursos;//array que contiene a los cursos
    String curso;
    SharedPreferences prefs;
    SharedPreferences.Editor editor;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ingresar_alumno);

        Toolbar  toolbar = (Toolbar) findViewById(R.id.menu2);
        setSupportActionBar(toolbar);

        //llenamos el array con los cursos
        cursos = new String[]{"Primero Básico","Segundo Básico","Tercero Básico","Cuarto Básico","Quinto Básico","Sexto Básico","Séptimo Básico","Octavo Básico"};

        ArrayAdapter<String> adaptador =
                new ArrayAdapter<String>(this,
                        android.R.layout.simple_spinner_item, cursos);

        spinnerCursos = (Spinner)findViewById(R.id.spinnerLayout);
        spinnerCursos.setAdapter(adaptador);
        spinnerCursos.setOnItemSelectedListener(this);

    }

    //metodo para ingresar los datos, se llama desde el boton subir del admin.xml
    public void ingresarAlumno(View view){
        rut = (EditText) findViewById(R.id.txtRutIngresado);//traemos el objeto rut
        nombre = (EditText) findViewById(R.id.txtNombre);//traemos el objeto nombre
        ciclo = (EditText) findViewById(R.id.txtCiclo);//traemos el objeto ciclo
        numeroLista = (EditText) findViewById(R.id.txtNumeroLista);
        //pasamos los datos ingresados a las variables correspondientes
        String rut1 = rut.getText().toString();
        String nombre1 = nombre.getText().toString();
        String ciclo1 = ciclo.getText().toString();
        String stringNumeroLista = numeroLista.getText().toString();

        DatabaseReference dbRef = FirebaseDatabase.getInstance().getReference().child("alumnos");

        Map<String, String> datos = new HashMap<>();
        datos.put("nombre", nombre1);
        datos.put("ciclo", ciclo1);
        datos.put("curso", curso);
        datos.put("numero lista",stringNumeroLista);
        dbRef.child(rut1).setValue(datos);

        DatabaseReference dd = FirebaseDatabase.getInstance().getReference().child("alumnos").child(rut1);
        Map<String, Integer> nota = new HashMap<>();
        nota.put("valor", 0);

        //introducimos diez notas en cada materia
        for(int i = 1; i<11; i++){
            dd.child("matemática").child("nota "+i).setValue(nota);
            dd.child("lenguaje").child("nota "+i).setValue(nota);
            dd.child("historia").child("nota "+i).setValue(nota);
            dd.child("ciencias").child("nota "+i).setValue(nota);
            dd.child("inglés").child("nota "+i).setValue(nota);
            dd.child("arte").child("nota "+i).setValue(nota);
            dd.child("música").child("nota "+i).setValue(nota);
            dd.child("tecnológica").child("nota "+i).setValue(nota);
            dd.child("religión").child("nota "+i).setValue(nota);
            dd.child("ed física").child("nota "+i).setValue(nota);
        }
        
        enviarMensaje();//entregamos mensaje de confirmacion
        limpiarCampos();//limpiamos campos
    }

    //método para avisar que el alumno se ingreso correctamente
    private void enviarMensaje(){
        Toast mensaje = Toast.makeText(getApplicationContext(),"Ingresado Correctamente", Toast.LENGTH_SHORT);
        mensaje.show();
    }

    //método para limpiar los datos una vez ingresados
    private void limpiarCampos(){
        //usamos el metodo clear de los editText
        nombre.getText().clear();
        rut.getText().clear();
        ciclo.getText().clear();
        numeroLista.getText().clear();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        curso = cursos[position];
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater menuInflater =  getMenuInflater();
        menuInflater.inflate(R.menu.main,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        int id = item.getItemId();
        if (id == R.id.cerrar_sesion){
            prefs = getSharedPreferences("MisPreferencias",Context.MODE_PRIVATE);
            editor = prefs.edit();
            editor.putBoolean("sesion", false);
            editor.commit();

            Intent intent = new Intent(this, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
