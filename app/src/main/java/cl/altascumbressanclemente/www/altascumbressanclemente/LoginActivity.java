package cl.altascumbressanclemente.www.altascumbressanclemente;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    FirebaseAuth firebaseAuth;
    FirebaseAuth.AuthStateListener firebaseAuthListener;
    GoogleSignInClient mGoogleSignInClient;
    DatabaseReference dt = FirebaseDatabase.getInstance().getReference();//objeto que almacena la referencia a la base de datos
    DatabaseReference s;
    String idUsuario = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        findViewById(R.id.boton_google).setOnClickListener(this);
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);


        firebaseAuth = FirebaseAuth.getInstance();

        //oyente para cuando tengamos un usuario autenticado con el método AuthStateListener:
        firebaseAuthListener = new FirebaseAuth.AuthStateListener(){
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser firebaseUser = firebaseAuth.getCurrentUser();
                //si hay un usuario abrimos la activity principal
                if(firebaseUser != null){
                    goMainActivity();
                }
            }
        };
    }

    //metodo que recibe la cuenta de google para autenticar con firebase
    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        firebaseAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (!task.isSuccessful()) {
                            registrarUsuario(null);
                        }else{
                            FirebaseUser user = firebaseAuth.getCurrentUser();
                            if(user == null) {
                                registrarUsuario(user);
                            }
                        }
                    }
                });
    }


    @Override
    protected void  onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 9001) {//si el metodo de autenticacion es google
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = task.getResult(ApiException.class);
                firebaseAuthWithGoogle(account);
            } catch (ApiException e) {
                System.out.println("Fallo al conectar con google");
            }
        }
    }


    //metodo para colocar un enlace a la pagina web del colegio
    /*
    private void crearBtnPaginaWeb() {
        TextView textView = (TextView) findViewById(R.id.textView4);
        textView.setClickable(true);
        textView.setMovementMethod(LinkMovementMethod.getInstance());
        String text = "<a href='http://www.altascumbressanclemente.cl/pass'> Obtener contraseña </a>";
        textView.setText(Html.fromHtml(text));
    }*/

    @Override
    protected void onStart(){
        super.onStart();
        firebaseAuth.addAuthStateListener(firebaseAuthListener);
        // Sign in success, update UI with the signed-in user's information



    }

    //metodo que se llama cuando se cierra la app
    @Override
    protected void onStop() {
        super.onStop();
        if(firebaseAuthListener != null) {//si el listener no es nulo
            firebaseAuth.removeAuthStateListener(firebaseAuthListener);//lo removemos del auth
        }
    }


    public void pulsoBotonEntrar(View view){
        onStart();//llamamos al metodo onStart para tener el dato de la pass de la BD
    }



    //iniciamos la actividad de inicio de sesion con Google esperando un resultado
    private void signIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, 9001);
    }


    private void registrarUsuario(FirebaseUser user) {
        if(user != null) {
            // Name, email address, and profile photo Url
            String name = user.getDisplayName();
            // Check if user's email is verified
            boolean emailVerified = user.isEmailVerified();

            // The user's ID, unique to the Firebase project. Do NOT use this value to
            // authenticate with your backend server, if you have one. Use
            // FirebaseUser.getIdToken() instead.
            String uid = user.getUid();
            s = dt.child("usuarios");//navegamos entre los nodos de la BD


            s.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange( DataSnapshot dataSnapshot) {
                    idUsuario = dataSnapshot.getValue().toString();
                }

                @Override
                public void onCancelled( DatabaseError databaseError) {
                    System.out.println("Error al leer contraseña LoginActivity :35");
                }
            });



            if(idUsuario != uid) {
                Map<String, String> datos = new HashMap<>();
                datos.put("nombre", name);
                datos.put("email", user.getEmail());
                s.child(uid).setValue(datos);
            }

        }
    }

    //metodo que escucha a los botones
    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.boton_google) {
            signIn();
        }
    }

    private void goMainActivity() {
        Intent intent = new Intent(this, UserActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }
}
