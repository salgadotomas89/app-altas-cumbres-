package cl.altascumbressanclemente.www.altascumbressanclemente;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.Map;

import cl.altascumbressanclemente.www.clases.AdaptadorMaterias;
import cl.altascumbressanclemente.www.clases.Materia;

public class MateriasActivity extends AppCompatActivity {
    Materia[] datos;
    String opcionSeleccionada;
    String rut,ciclo,curso,nombre;
    TextView nombreTextView, cursoTextView;

    DatabaseReference dt = FirebaseDatabase.getInstance().getReference();//objeto que almacena la referencia a la base de datos
    DatabaseReference s;
    String idUsuario = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_materias);

        Toolbar toolbar = (Toolbar) findViewById(R.id.menu);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    @Override
    protected void onResume(){
        super.onResume();

        nombre = getIntent().getExtras().getString("nombre");
        rut = getIntent().getExtras().getString("rut");
        ciclo = getIntent().getExtras().getString("ciclo");
        curso = getIntent().getExtras().getString("curso");


        nombreTextView = (TextView) findViewById(R.id.textView7);
        cursoTextView = (TextView) findViewById(R.id.textView9);
        nombreTextView.setText(getIntent().getExtras().getString("nombre"));
        cursoTextView.setText(getIntent().getExtras().getString("curso"));


        int n = Integer.parseInt(ciclo);

        if(n == 1){
            if(curso.equals("Primero Básico")) {
                datos = new Materia[]{
                        new Materia("Matemática", "Juan Pablo Carreño"),
                        new Materia("Lenguaje", "Juan Pablo Carreño"),
                        new Materia("Historia", "Juan Pablo Carreño"),
                        new Materia("Ciencias", "Juan Pablo Carreño"),
                        new Materia("Ed Física", "Gonzalo Sanhueza"),
                        new Materia("Inglés", "Nathaly Quinteros"),
                        new Materia("Tecnológica", "Juan Pablo Carreño"),
                        new Materia("Arte", "Juan Pablo Carreño"),
                        new Materia("Religión", "Juan Pablo Carreño"),
                        new Materia("Música", "Ignacio Moya")
                };
            }else{
                datos = new Materia[]{
                        new Materia("Matemática", "Patricia Contreras "),
                        new Materia("Lenguaje", "Patricia Contreras"),
                        new Materia("Historia", "Patricia Contreras"),
                        new Materia("Ciencias", "Patricia Contreras"),
                        new Materia("Ed Física", "Gonzalo Sanhueza"),
                        new Materia("Inglés", "Nathaly Quinteros"),
                        new Materia("Tecnológica", "Patricia Contreras"),
                        new Materia("Arte", "Patricia Contreras"),
                        new Materia("Religión", "Patricia Contreras"),
                        new Materia("Música", "Ignacio Moya")
                };
            }
        }else {
            datos = new Materia[]{
                    new Materia("Matemática", "Haron Cubillos"),
                    new Materia("Lenguaje", "Paulina Mella"),
                    new Materia("Historia", "Alberto Morales"),
                    new Materia("Ciencias", "Marcelo Araya"),
                    new Materia("Ed Física", "Gonzalo Sanhueza"),
                    new Materia("Inglés", "Nathaly Quinteros"),
                    new Materia("Tecnológica", "Francisco Moya"),
                    new Materia("Arte", "Francisco Moya"),
                    new Materia("Religión", "Karina Valdes"),
                    new Materia("Música", "Ignacio Moya")
            };
        }



        AdaptadorMaterias adaptador =
                new AdaptadorMaterias(this, datos);

        ListView lstOpciones = (ListView)findViewById(R.id.LstOpciones);

        lstOpciones.setAdapter(adaptador);

        lstOpciones.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> a, View v, int position, long id) {

                opcionSeleccionada = ((Materia)a.getItemAtPosition(position)).getNombre();

                lanzarNotas();
            }
        });
    }

    @Override
    protected void onRestart(){
        super.onRestart();
    }

    @Override
    protected void onStop(){
        super.onStop();
    }




    private void lanzarNotas(){
        Intent nuevo = new Intent(this, NotasActivity.class);
        nuevo.putExtra("rut",rut);
        nuevo.putExtra("materia",opcionSeleccionada);
        startActivity(nuevo);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater menuInflater =  getMenuInflater();
        menuInflater.inflate(R.menu.menu2,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == R.id.agregarAlumno){
            FirebaseAuth     firebaseAuth = FirebaseAuth.getInstance();

            FirebaseUser user = firebaseAuth.getCurrentUser();

            s = dt.child("usuarios");//navegamos entre los nodos de la BD
            Map<String, String> datos = new HashMap<>();
            datos.put("nombre", nombre);
            datos.put("rut", rut);
            s.child(user.getUid()).child("hijos").child(rut).setValue(datos);
            enviarMensaje();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    private void enviarMensaje(){
        Toast mensaje = Toast.makeText(getApplicationContext(),"Ingresado a su mochila", Toast.LENGTH_SHORT);
        mensaje.show();
    }
}
