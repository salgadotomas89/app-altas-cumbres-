package cl.altascumbressanclemente.www.altascumbressanclemente;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.DataSet;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import cl.altascumbressanclemente.www.clases.Nota;
import cl.altascumbressanclemente.www.clases.NotaHolder;

public class NotasActivity extends AppCompatActivity {

    TextView lblPromedio;
    double sumaDeNotas=0;
    double contador=0;
    double prom;
    FirebaseRecyclerAdapter mAdapter;
    CardView cardView;

    private BarChart barChart;
    private List<String> etiquetas;
    private List <Integer> notas ;
    private int indiceNota;//indice para la etiqueta nota ?
    RecyclerView recycler;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notas);

        String m = getIntent().getExtras().getString("materia");
        String materia = m.toLowerCase();

        cardView = (CardView) findViewById(R.id.card2);

        indiceNota = 0;
        lblPromedio = (TextView) findViewById(R.id.txtProm);
        notas = new ArrayList<>();
        etiquetas =  new ArrayList<>();
        for(int i=1;i<11;i++){
            etiquetas.add("Nota "+i+":");
        }

        String rut = getIntent().getExtras().getString("rut");

        final DatabaseReference dbNotas =
                FirebaseDatabase.getInstance().getReference().child("alumnos")
                        .child(rut).child(materia);

        recycler = (RecyclerView) findViewById(R.id.listaNotas);
        recycler.setHasFixedSize(true);
        recycler.setLayoutManager(new LinearLayoutManager(this));
        mAdapter =
                new FirebaseRecyclerAdapter<Nota, NotaHolder>(
                        Nota.class, R.layout.lista_notas, NotaHolder.class, dbNotas) {

                    @Override
                    public void populateViewHolder(NotaHolder predViewHolder, Nota pred, int position) {
                        if(pred.getvalor()==0){
                            if(indiceNota == 0) {
                                predViewHolder.setNota("Alumno no registra notas en esta asignatura");
                                indiceNota++;
                            }
                        }else {
                            double nota = pred.getvalor();
                            guardarNota(nota);
                            String notaText = String.valueOf(nota);
                            predViewHolder.setNota(notaText);
                            predViewHolder.setNombreNota(etiquetas.get(indiceNota));
                            indiceNota++;
                        }
                        mostrarPromedio();
                        barChart = (BarChart) findViewById(R.id.grafica);
                        crearGrafico();
                    }
                };

        recycler.setAdapter(mAdapter);
        recycler.setLayoutManager(new LinearLayoutManager(this));
    }


    private void mostrarPromedio() {
        if(contador > 1 ) {
            prom = sumaDeNotas/contador;
            if(prom >= 40){
                int lime=getResources().getColor(R.color.promAzul);
                cardView.setCardBackgroundColor(lime);
            }else{
                //myColor = getResources().getColor(R.color.promRojo);
                int lime=getResources().getColor(R.color.promRojo);
                cardView.setCardBackgroundColor(lime);
            }
            DecimalFormat df = new DecimalFormat("#.00");
            lblPromedio.setText("Promedio : " +df.format(prom));
        }else{
            lblPromedio.setText("Promedio : No hay suficientes notas");
        }
    }

    private void guardarNota(double notaDouble){
        double nota = notaDouble;
        int notaInt = (int) nota;
        notas.add(notaInt);
        sumaDeNotas += nota;
        contador++;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mAdapter.cleanup();
    }

    private void crearGrafico(){

        float groupSpace = 0.06f;
        float barSpace = 0.02f; // x2 dataset
        float barWidth = 0.45f; // x2 dataset

        barChart.animateY(1500);
        barChart.setDrawGridBackground(false);
        barChart.setData(getBarData());
        //barChart.setBackgroundColor(Color.WHITE);
        barChart.getDescription().setText("");

        barChart.invalidate();
        barChart.setFitBars(true);//el chart se ajusta a las barras

        Legend legend = barChart.getLegend();
        legend.setEnabled(false);
        ejeX(barChart.getXAxis());
        ejeYDerecho(barChart.getAxisRight());
        ejeYIzquierdo(barChart.getAxisLeft());
    }

    private DataSet getData(DataSet dataSet){
        dataSet.setValueTextSize(12);
        //dataSet.setColor(Color.RED);
        return dataSet;
    }

    private BarData getBarData(){
        BarDataSet barDataSet = (BarDataSet) getData(new BarDataSet(getBarEntries(),""));
        if(prom >= 40){
            int lime=getResources().getColor(R.color.promAzul);
            barDataSet.setColor(lime);
        }else{
            //myColor = getResources().getColor(R.color.promRojo);
            int lime=getResources().getColor(R.color.promRojo);
            barDataSet.setColor(lime);
        }
        //BarData barData = new BarData(barDataSet);
        // initialize the Bardata with argument labels and dataSet
        BarData data = new BarData(barDataSet);

        data.setBarWidth(0.9f); // set custom bar width



        return  data;
    }


    private ArrayList<BarEntry> getBarEntries(){
        ArrayList<BarEntry> entries =  new ArrayList<>();
        for(int i=0;i<notas.size();i++){
            entries.add(new BarEntry(i, notas.get(i)));
        }
        return entries;
    }


    private void ejeX(XAxis eje){
        eje.setGranularityEnabled(true);
        eje.setPosition(XAxis.XAxisPosition.BOTTOM);
        eje.setValueFormatter(new IndexAxisValueFormatter(etiquetas));
        eje.setTextSize(8);
    }

    private void ejeYDerecho(YAxis ejeY){
        ejeY.setEnabled(false);
    }

    private void ejeYIzquierdo(YAxis ejeY){
        ejeY.setSpaceTop(0);
        ejeY.setAxisMinimum(10);
        ejeY.setAxisMaximum(70);
    }
}
