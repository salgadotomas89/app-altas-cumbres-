package cl.altascumbressanclemente.www.altascumbressanclemente;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

public class PrincipalActivity extends AppCompatActivity {
    SharedPreferences prefs;
    SharedPreferences.Editor editor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal);

        Toolbar toolbar = (Toolbar) findViewById(R.id.menu2);
        setSupportActionBar(toolbar);
    }

    //metodo que inicia la activity para ingresar alumno
    public void ingresarAlumno(View view){
        Intent intent =  new Intent(this, IngresarAlumnoActivity.class);
        startActivity(intent);
    }

    public void ingresarNotaAlumno(View view){
        Intent intent =  new Intent(this, IngresarNotaAlumnoActivity.class);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater menuInflater =  getMenuInflater();
        menuInflater.inflate(R.menu.main,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == R.id.cerrar_sesion) {
            prefs = getSharedPreferences("MisPreferencias",Context.MODE_PRIVATE);
            editor = prefs.edit();
            editor.putBoolean("sesion", false);
            editor.apply();

            Intent intent = new Intent(this, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);

            return true;

        }
        return super.onOptionsItemSelected(item);
    }

}
