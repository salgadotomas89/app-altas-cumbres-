package cl.altascumbressanclemente.www.altascumbressanclemente;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import cl.altascumbressanclemente.www.clases.ViewPagerAdapter;

public class UserActivity extends AppCompatActivity {
    String rutAlumno;
    String ciclo,nombre,curso;
    String name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);

        Toolbar toolbar = (Toolbar) findViewById(R.id.menu);
        setSupportActionBar(toolbar);

        ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(viewPagerAdapter);
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
    }




    public void pulsoBtnBuscarAlumno(View view){

        EditText editT = (EditText) findViewById(R.id.textoIngresado);//nombre ingresado
        rutAlumno = editT.getText().toString();//lo convertimos a string
        //rutAlumno = nombreSinEditar.toLowerCase();

        DatabaseReference myRef = FirebaseDatabase.getInstance().getReference().child("alumnos").child(rutAlumno);

            ValueEventListener eventListener = new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if(dataSnapshot.getValue() != null){
                         ciclo = dataSnapshot.child("ciclo").getValue().toString();
                         nombre = dataSnapshot.child("nombre").getValue().toString();
                         curso = dataSnapshot.child("curso").getValue().toString();
                         crearNuevoActivity();

                    }else{
                        Toast mensaje
                                = Toast.makeText(getApplicationContext(),"No se encuentra alumno", Toast.LENGTH_SHORT);
                        mensaje.show();
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            };
            myRef.addListenerForSingleValueEvent(eventListener);
            myRef.removeEventListener(eventListener);


    }

    private void crearNuevoActivity() {
        Intent nuevo = new Intent(this,MateriasActivity.class);
        nuevo.putExtra("rut", rutAlumno);
        nuevo.putExtra("nombre", nombre);
        nuevo.putExtra("curso", curso);
        nuevo.putExtra("ciclo",ciclo);
        startActivity(nuevo);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater menuInflater =  getMenuInflater();
        menuInflater.inflate(R.menu.main,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.cerrar_sesion){
            FirebaseAuth.getInstance().signOut();
            goLoginActivity();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void goLoginActivity() {
        Intent intent = new Intent(this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }



}
