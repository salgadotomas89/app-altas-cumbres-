package cl.altascumbressanclemente.www.clases;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import cl.altascumbressanclemente.www.altascumbressanclemente.R;

public class AdaptadorMaterias extends ArrayAdapter<Materia> {
    Materia[] datos;

    public AdaptadorMaterias(Context context, Materia[] datos) {
        super(context, R.layout.lista_materias, datos);
        this.datos = datos;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View item = inflater.inflate(R.layout.lista_materias, null);

        TextView lblTitulo = (TextView)item.findViewById(R.id.lblNombreMateria);
        lblTitulo.setText(datos[position].getNombre());

        TextView lblNombre = (TextView)item.findViewById(R.id.lblProfesor);
        lblNombre.setText(datos[position].getNombreProfesor());

        return(item);
    }
}
