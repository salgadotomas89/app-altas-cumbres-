package cl.altascumbressanclemente.www.clases;

public class Alumno {
    String rut;
    String nombre;

    public Alumno(){

    }

    public Alumno(String rut,String nombre){
        this.rut = rut;
        this.nombre = nombre;
    }
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    @Override
    public String toString() {
        return "Alumno{" +
                ", rut=" + rut +
                '}';
    }
}
