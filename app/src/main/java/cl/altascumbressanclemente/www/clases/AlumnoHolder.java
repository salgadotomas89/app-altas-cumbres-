package cl.altascumbressanclemente.www.clases;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import cl.altascumbressanclemente.www.altascumbressanclemente.R;

public class AlumnoHolder extends RecyclerView.ViewHolder {

    private View mView;

    public  AlumnoHolder(View itemView) {
        super(itemView);
        mView = itemView;
    }

    public void setRut(String nombre) {
        Button field = (Button) mView.findViewById(R.id.button8);
        field.setText(nombre);
    }

    public void setNombre(String nombre) {
        Button field = (Button) mView.findViewById(R.id.button8);
        field.setText(nombre);
    }
}
