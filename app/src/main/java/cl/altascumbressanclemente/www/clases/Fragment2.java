package cl.altascumbressanclemente.www.clases;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;

import cl.altascumbressanclemente.www.altascumbressanclemente.R;


public class Fragment2 extends Fragment {
    RecyclerView recycler;
    FirebaseRecyclerAdapter mAdapter;
    FirebaseAuth firebaseAuth;
    FirebaseUser user;
    List<Alumno> datos;

    public Fragment2() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        datos = new ArrayList<>();
        datos.add(new Alumno("17182828","tomas "));
        datos.add(new Alumno("1718181","juana"));

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        /*
        firebaseAuth = FirebaseAuth.getInstance();

        user = firebaseAuth.getCurrentUser();

        DatabaseReference dbNotas =
                FirebaseDatabase.getInstance().getReference().child("usuarios").child(user.getUid()).child("hijos");

        */
        View v = inflater.inflate(R.layout.fragment2, container,false);

        recycler =  v.findViewById(R.id.hijosRecycler);
        RecyclerViewAdapter recyclerViewAdapter = new RecyclerViewAdapter(getContext(),datos);
        recycler.setLayoutManager(new LinearLayoutManager(getContext()));

        /*
        mAdapter =
                new FirebaseRecyclerAdapter<Alumno, AlumnoHolder>(
                        Alumno.class, R.layout.lista_hijos, AlumnoHolder.class, dbNotas){

                    @Override
                    public void populateViewHolder(AlumnoHolder predViewHolder, Alumno pred, int position) {
                        if(pred.getRut()!=null){
                            String rut = pred.getRut();
                            String nombre = pred.getNombre();
                            predViewHolder.setNombre(nombre);
                        }
                    }
                };*/
        recycler.setAdapter(recyclerViewAdapter);

        return v;
    }


}
