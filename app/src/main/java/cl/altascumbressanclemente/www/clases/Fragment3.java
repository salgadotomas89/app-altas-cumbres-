package cl.altascumbressanclemente.www.clases;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import cl.altascumbressanclemente.www.altascumbressanclemente.R;

public class Fragment3 extends Fragment {
    FirebaseAuth firebaseAuth;
    FirebaseUser user;

    public Fragment3(){
        firebaseAuth = FirebaseAuth.getInstance();


        user = firebaseAuth.getCurrentUser();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View myInflatedView = inflater.inflate(R.layout.fragment3, container,false);
        TextView t = (TextView) myInflatedView.findViewById(R.id.nombreUsuario);
        t.setText(user.getDisplayName());
        return myInflatedView;

    }
}
