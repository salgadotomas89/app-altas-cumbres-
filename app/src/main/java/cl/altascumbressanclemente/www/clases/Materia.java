package cl.altascumbressanclemente.www.clases;

public class Materia {
    String nombre;
    String nombreProfesor;

    public  Materia(){

    }

    public Materia(String nombre, String nombreProfe){
        this.nombre = nombre;
        this.nombreProfesor = nombreProfe;
    }

    public void setNombre(String nombre){
        this.nombre = nombre;
    }

    public String getNombre(){
        return nombre;
    }

    public String getNombreProfesor() {
        return nombreProfesor;
    }

    public void setNombreProfesor(String nombreProfesor) {
        this.nombreProfesor = nombreProfesor;
    }
}
