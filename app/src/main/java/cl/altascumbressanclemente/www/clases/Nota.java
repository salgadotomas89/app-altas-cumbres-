package cl.altascumbressanclemente.www.clases;

import java.util.ArrayList;
import java.util.List;

public class Nota {
    private double valor;

    public Nota() {
        //Es obligatorio incluir constructor por defecto
    }

    public Nota( double valor)
    {
        this.valor = valor;
    }

    public double getvalor() {
        return valor;
    }

    public void setvalor(float valor) {
        this.valor = valor;
    }

    @Override
    public String toString() {
        return "Nota{" + ", valor=" + valor + '}';
    }
}
