package cl.altascumbressanclemente.www.clases;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import cl.altascumbressanclemente.www.altascumbressanclemente.R;

public class NotaHolder extends RecyclerView.ViewHolder {
        private View mView;

        public NotaHolder(View itemView) {
                super(itemView);
                mView = itemView;
        }

        public void setNota(String temp) {
                TextView field = (TextView) mView.findViewById(R.id.lblValorNota);
                field.setText(temp);
        }

        public void setNombreNota(String nombreNota) {
                TextView field = (TextView) mView.findViewById(R.id.lblNota);
                field.setText(nombreNota);
        }
}
