package cl.altascumbressanclemente.www.clases;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.util.List;

import cl.altascumbressanclemente.www.altascumbressanclemente.R;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.MyViewHolder> {

    Context context;
    List<Alumno> mData;

    public RecyclerViewAdapter(Context context, List<Alumno> mdata){
        this.context = context;
        this.mData = mdata;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View v;
        v = LayoutInflater.from(context).inflate(R.layout.lista_hijos,parent,false);
        MyViewHolder myViewHolder = new MyViewHolder(v);

        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {
        myViewHolder.btnHijo.setText(mData.get(i).getNombre());

    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder{
        Button btnHijo;

        public MyViewHolder(View itemView){
            super(itemView);
            btnHijo = itemView.findViewById(R.id.button8);
        }
    }
}
