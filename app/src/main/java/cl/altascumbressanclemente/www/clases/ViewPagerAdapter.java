package cl.altascumbressanclemente.www.clases;



import android.content.Context;
import android.content.SharedPreferences;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class ViewPagerAdapter extends FragmentPagerAdapter {

    public ViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        if (position == 0){
            fragment = new Fragment1();
        }else if (position == 1){
            fragment = new Fragment2();
        }else if (position == 2){
            fragment = new Fragment3();
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        String title = null;
        if (position == 0)
        {
            title = "Busqueda";
        }
        else if (position == 1)
        {
            title = "Mi Mochila";
        }else if (position == 2)
        {
            title = "Perfil";
        }

        return title;
    }
}

